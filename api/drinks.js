export const getAllDrinks = () => {
    return fetch('https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=Gin')
            .then(response => response.json())
            .then(data => data.drinks)
    // Promise
}

export const getDrinkById = id => {
    return fetch('https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=' + id)
            .then(response => response.json())
            .then(data => {
               if (data.drinks === null) {
                   return [];
               }
               return data.drinks;
            })
            .then(drinks => {
                if (drinks.length > 0) {
                    return drinks[0];
                } 
                throw Error('Drink not found')
            })
}