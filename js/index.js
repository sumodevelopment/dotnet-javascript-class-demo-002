import { getAllDrinks } from '../api/drinks.js';

// Functional Constructor -> Kinda like a class
function App() {
    this.elStatus = document.getElementById('status');
    this.elDrinkList = document.getElementById('drinks');
    this.error = '';
    this.drinks = [];
    return this;
}

// Attach methods to prototype -> Better for Memory IF Multiple instances of App exist.
App.prototype.init = async function() {
    this.elStatus.innerText = 'Loading drinks...';
    await this.getDrinks();
}

// Async function to get drinks from an external endpoint.
App.prototype.getDrinks = async function() {
    try {
         this.drinks = await getAllDrinks();
    } catch (e) {
        this.error = e.message;
    } finally {
        this.render();
    }
}; 

App.prototype.handleDrinkClicked = function(drinkId) {
    window.location.href = 'drink.html?d=' + drinkId;
}

// Logic to display stuff on the DOM.
App.prototype.render = function() {

    // Negative check - Assume there is an error
    if (this.error) { 
        this.elStatus.innerText = this.error;
        // Return to avoid any remaining code to execute.
        return;
    }

    // Code here is safe because there was no error.

    // Loop over drinks
    this.drinks.forEach(drink => {
        // Create a parent Drink list item.
        const elDrink = document.createElement('li');
        elDrink.className = 'drink-list-item';

        // Create a drink image element
        const elImgContainer = document.createElement('div');
        elImgContainer.className = 'drink-list-item_thumb';
        const img = new Image();
        img.className = 'drink-list-item_thumb-img'
        img.onload = function() {
            // It's loaded and ready for the dom.
            elImgContainer.appendChild( img );
        }
        img.src = drink.strDrinkThumb;
        elDrink.appendChild( elImgContainer );

        // Create a drink name element.
        const elDrinkName = document.createElement('h4');
        elDrinkName.className = 'drink-list-item_title';
        elDrinkName.innerText = drink.strDrink;
        elDrink.appendChild( elDrinkName );

        elDrink.addEventListener('click', (e) => {
            e.preventDefault();
            this.handleDrinkClicked(drink.idDrink);
        });

        // Append the drink Parent li item to the main drink list. ul#drinks
        this.elDrinkList.appendChild( elDrink );
    });

    this.elStatus.innerText = '';
}

// Instantiate the app.
new App().init();