import { getDrinkById } from '../api/drinks.js';

class Cocktail {
    constructor() {
        this.elDrinkName = document.getElementById('drink-name');
        this.elDrinkContainer = document.getElementById('drink');
        this.elStatus = document.getElementById('status');
        this.error = '';
        this.drinkId = 0;
        this.drink = null;
        return this;
    }

    async init() {
        const params = new URLSearchParams(window.location.search);
        this.drinkId = params.get('d');
        await this.getDrink(this.drinkId);
    }

    async getDrink(id) {
        try {
            this.drink = await getDrinkById(id);
        } catch(e) {
            this.error = e.message;
        } finally {
            this.render();
        }
    }

    render() {
        if (this.error) {
            this.elStatus.innerText = this.error;
            return;
        }

        // Title 
        this.elDrinkName.innerText = this.drink.strDrink;

        // Image
        const elImgContainer = document.createElement('div');
        elImgContainer.className = 'drink-detail_thumb';
        const img = new Image();
        img.width = 200;
        img.onload = function() {
            elImgContainer.appendChild( img );
        }
        img.src = this.drink.strDrinkThumb;
        this.elDrinkContainer.appendChild( elImgContainer );

        // Is Alcoholic
        const elIsAlcoholic = document.createElement('p');
        elIsAlcoholic.innerText = this.drink.strAlcoholic;
        this.elDrinkContainer.appendChild( elIsAlcoholic );

        // Glass type
        const elGlassType = document.createElement('p');
        elGlassType.innerText = 'Glass type: ' + this.drink.strGlass;
        this.elDrinkContainer.appendChild( elGlassType );

        // Category
        const elCategory = document.createElement('p');
        elCategory.innerText = 'Category: ' + this.drink.strCategory;
        this.elDrinkContainer.appendChild( elCategory );

         // Instructions
         const elInstructionsHeading = document.createElement('h4');
         elInstructionsHeading.innerText = 'Instructions';
         this.elDrinkContainer.appendChild( elInstructionsHeading );
         
         const elInstructions = document.createElement('p');
         elInstructions.innerText = this.drink.strInstructions;
         this.elDrinkContainer.appendChild( elInstructions );

         this.elStatus.innerText = '';
    }
}



new Cocktail().init();